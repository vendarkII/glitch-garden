﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;

public class OptionsManager : MonoBehaviour {

	public Slider volSlider;
	public Slider diffSlider;
	public LevelManager levelManager;
	private MusicManager musicManger;

	// Use this for initialization
	void Start() {
		musicManger = GameObject.FindObjectOfType<MusicManager>();
		volSlider.value = PlayerPrefsManager.getMasterVolume();
		diffSlider.value = PlayerPrefsManager.getDifficulty();
	}

	public void saveAndExit() {
		PlayerPrefsManager.setMasterVolume(volSlider.value);
		PlayerPrefsManager.setDifficulty(diffSlider.value);
		levelManager.LoadLevel("01a Start");
	}

	public void setDefaults() {
		volSlider.value = 0.5f;
		diffSlider.value = 2f;
	}
	
	// Update is called once per frame
	void Update() {
		musicManger.changeVolume(volSlider.value);
	}
}
