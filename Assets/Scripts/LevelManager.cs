﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	public float after;

	void Start() {
		if (SceneManager.GetActiveScene().buildIndex == 0)
			Invoke("LoadNextLevel", after);
	}

	public void LoadLevel(string name) {
		Debug.Log("Level load requested for: " + name);
		Application.LoadLevel(name);
	}

	public void LoadNextLevel() {
		Application.LoadLevel(Application.loadedLevel + 1);
	}

	public void QuitRequest() {
		Debug.Log("Quit requested");
		Application.Quit();
	}

}
