﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadingManager : MonoBehaviour {

	public bool fadeIn;
	public float time;
	public LevelManager levelManager;

	private Image img;
	private Color currentColor;

	// Use this for initialization
	void Start() {
		img = GetComponent<Image>();
		currentColor = img.color;
	}

	// Update is called once per frame
	void Update() {
		if (fadeIn) {
			if (Time.timeSinceLevelLoad < time) {
				currentColor.a -= Time.deltaTime / time;
				img.color = currentColor;
			} else {
				gameObject.SetActive(false);
			}
		} else {
			if (Time.timeSinceLevelLoad < levelManager.after && Time.timeSinceLevelLoad > time) {
				currentColor.a += Time.deltaTime / (levelManager.after - time);
				img.color = currentColor;
			}
		}
	}
}
