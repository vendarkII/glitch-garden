﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {
	public AudioClip[] levelMusicChangeArray;
	private AudioSource audioSource;

	void Awake() {
		DontDestroyOnLoad(gameObject);
		Debug.Log("Don't destroy on load " + name);
		audioSource = gameObject.AddComponent<AudioSource>();
		audioSource.volume = PlayerPrefsManager.getMasterVolume();
	}

	void OnLevelWasLoaded() {
		if (SceneManager.GetActiveScene().buildIndex != 1 && SceneManager.GetActiveScene().buildIndex != 2 && SceneManager.GetActiveScene().buildIndex != 4 && SceneManager.GetActiveScene().buildIndex != 5) {
			if (audioSource.isPlaying)
				audioSource.Stop();
			audioSource.clip = levelMusicChangeArray[SceneManager.GetActiveScene().buildIndex];
			if (SceneManager.GetActiveScene().buildIndex == 6 || SceneManager.GetActiveScene().buildIndex == 7) {
				audioSource.loop = false;
			} else {
				audioSource.loop = true;
			}
			audioSource.Play();
		} else if (SceneManager.GetActiveScene().buildIndex == 1 && !audioSource.isPlaying) {
			audioSource.clip = levelMusicChangeArray[1];
			audioSource.loop = true;
			audioSource.Play();
		} else if (audioSource.isPlaying && audioSource.clip == levelMusicChangeArray[6]) {
			audioSource.Stop();
			audioSource.clip = levelMusicChangeArray[1];
			audioSource.loop = true;
			audioSource.Play();
		}
	}

	public void changeVolume(float volume) {
		audioSource.volume = volume;
	}
}